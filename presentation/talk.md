---
title: Gitlab Tips
date: 21.02.2022
---

## Schnellere Pipelines

- Vermeiden wiederholter Dependency-Installation
- Artifacts: Zwischenspeichern innerhalb einer Pipeline
- Cache: Zwischenspeichern über Pipeline-Runs hinaus

---

## Artifacts

``` yaml
install_dependencies:
  stage: prepare
  script:
    - yarn install --frozen-lockfile
  artifacts:
    paths:
      - node_modules

test:
  stage: test
  script:
    - yarn test
```

- Dependencies am Anfang der Pipeline installieren
- Alle Jobs können die Dependencies nutzen

---

## Cache

``` yaml
install_dependencies:
  stage: prepare
  cache:
    key:
      files:
        - yarn.lock
    paths:
      - node_modules
  script:
    - yarn install --frozen-lockfile

test:
  stage: test
  cache:
    key:
      files:
        - yarn.lock
    paths:
      - node_modules
  script:
    - yarn test
```

- Bei mehreren Runnern muss ein remote Cache verfügbar sein

---

## Cache + Artifacts

``` yaml
install_dependencies:
  stage: prepare
  cache:
    key:
      files:
        - yarn.lock
    paths:
      - node_modules
  script:
    - yarn install --frozen-lockfile
  artifacts:
    paths:
      - node_modules

test:
  stage: test
  script:
    - yarn test
```

- Cache für das initiale Installieren
- Artifacts für den Rest der Pipeline

---

## Feature Flags

``` yaml
variables:
  FF_USE_FASTZIP: "true"
  ARTIFACT_COMPRESSION_LEVEL: "fast"
  CACHE_COMPRESSION_LEVEL: "fast"
```

- Fastzip für schnellere Compression
- Compression level als Geschwindigkeit/Größe Tradeoff

---

## Schnellere Pipelines - Ergebnis
Dependency-Installation per Job:
![Dependency-Installation per Job](images/slow-pipeline.png)
Dependency-Installation einmalig mit Cache und Artifacts:
![Dependency-Installation einmalig mit Cache und Artifacts](images/fast-pipeline.png)

---

## Caveats

- `npm ci` löscht node_modules vor der Installation
- `npm install` ist für Pipelines ungeeignet
- -> Dependency Caching nur mit yarn sinvoll
- für Cypress: `yarn cypress install` im e2e-Job

---

## Review Apps

- Dynamische Deployments für Feature-Branches
- Können vom Merge Request per Button erreicht werden

---

## Review Apps benutzen

``` yaml
deploy_review:
  stage: review
  script:
    - ... # Deployment
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_COMMIT_REF_SLUG.example.com
```

- `$CI_COMMIT_REF_NAME`

   Branch- oder Tag
- `$CI_COMMIT_REF_SLUG`


   Branch- oder Tag: lowercase, nur `[a-z]`, `[0-9]` und `-`

---

## Review Apps stoppen/löschen

``` yaml
deploy_review:
  stage: review
  script:
    - ... # Deployment
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_ENVIRONMENT_SLUG.example.com
    on_stop: stop_review

stop_review:
  stage: review
  when: manual
  script:
    - ... # Clean up Deployment
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop

```

---

## Review Apps - Route Maps

<small>`.gitlab/route-map.yml`</small>
``` yaml
- source: /src\/elements\/(.*).vue/
  public: '/#/Elements/\1'
- source: /src\/patterns\/(.*).vue/
  public: '/#/Patterns/\1'
- source: /src\/templates\/(.*).vue/
  public: '/#/Templates/\1'
```

- `source`: Regex für veränderte Dateien
- `public`: Entsprechende Route, mit Regex Capture Groups

---

## Reports

- Coverage
- Testergebnisse
- Beliebige Artefakte

---

## Coverage
``` yaml
test:
  stage: test
  coverage: '/All files[^|]*\|[^|]*\s+([\d\.]+)/'
  script:
    - yarn test:ci
```

- `coverage`: Regex für coverage aus den Logs

---

## Coverage-Visualisierung

``` yaml
test:
  image: node:16-alpine
  stage: test
  script:
    - yarn test:ci
  artifacts:
    reports:
      cobertura: coverage/cobertura-report.xml
```

- Visualisiert Coverage im Merge Request Diff

---

## Test-Ergebnisse

``` yaml
test:
  image: node:16-alpine
  stage: test
  script:
    - yarn test:ci
  artifacts:
    reports:
      junit: junit.xml
```


- Detaillierte Auflistung der Testergebnisse im Merge Request

---

## Exposed Artifacts

``` yaml
e2e:
  image: cypress/browsers:node14.15.0-chrome96-ff94
  stage: e2e
  script:
    - yarn cypress install
    - yarn start &
    - yarn e2e
  artifacts:
    expose_as: 'video'
    paths:
        - cypress/videos/spec.ts.mp4
```

- Wird per `expose_as` im Merge Request angezeigt
- Keine Wildcards/Kein Globbing

---

## Gitlab config editieren/testen

- Yaml Language Server
- Gitlab Pipeline Editor
- gitlab-ci-local

---

## Links

- Gitlab Docs:

  [https://docs.gitlab.com/](https://docs.gitlab.com/)

- Gitlab artifacts reports:

  [https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html)

- gitlab-ci-local:

  [https://github.com/firecow/gitlab-ci-local](https://github.com/firecow/gitlab-ci-local)
  
- Yaml language server:

  [https://github.com/redhat-developer/yaml-language-server](https://github.com/redhat-developer/yaml-language-server)

---

## Fin
